package controller;

import com.sun.org.apache.xpath.internal.objects.XString;
import dao.Etudiant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import services.ImplEtudiantService;

import java.util.List;

@Controller
public class EtudiantController {

    @Autowired
    private ImplEtudiantService service;

    @RequestMapping("/")
    public String home() {
        return "index";
    }
    @RequestMapping("/index")
    private String index() {
        return "index";
    }

    @RequestMapping("/saisir")
    private String saisir() {
        return "saisir";
    }

    @RequestMapping("/affiche")
    private String affiche(Model model) {
        List<Etudiant> list = service.getAllEtudiant();
        model.addAttribute("liste", list);
        return "affiche";
    }

    @RequestMapping("/save")
    private String save(long code, String nom, String prenom, int age, String ville) {
        Etudiant etudiant = new Etudiant(code, nom, prenom, age, ville);
        service.addEtudiant(etudiant);
        return "affiche";
    }

    @RequestMapping("/delete")
    private String delete(Model model, @RequestParam("code") long code) {
        service.deleteEtudiant(code);

        List<Etudiant> list = service.getAllEtudiant();
        model.addAttribute("liste", list);
        return "affiche";
    }
}
