package dao;

import java.util.List;

public interface EtudiantDAO {

    Etudiant addEtudiant(Etudiant e);
    void  deleteEtudiant(long code);
    Etudiant updateEtudiant(Etudiant e);
    Etudiant getEtudiantByCode(long code);
    List<Etudiant> getAllEtudiant();
}
