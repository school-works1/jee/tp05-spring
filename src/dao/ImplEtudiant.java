package dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ImplEtudiant implements EtudiantDAO{
    private final static List<Etudiant> list = new ArrayList<>();

    public ImplEtudiant() {
        for (int i = 0; i < 3; i++)
            list.add(new Etudiant(i, "nom" + i, "prenom" + i, i*2, "ville" +i));
    }

    public void initialiser() {

    }

    @Override
    public Etudiant addEtudiant(Etudiant e) {
        list.add(e);
        return e;
    }

    @Override
    public void deleteEtudiant(long code) {
        list.removeIf(e -> e.getCode() == code);
    }

    @Override
    public Etudiant updateEtudiant(Etudiant e) {
        Optional<Etudiant> found =
                list.stream().filter(etudiant -> etudiant.getCode() == e.getCode()).findFirst();
        if ( found.isPresent() ) {
            found.get().setNom(e.getNom());
            found.get().setPrenom(e.getPrenom());
            found.get().setAge(e.getAge());
            found.get().setVille(e.getVille());
            return e;
        }
        return null;
    }

    @Override
    public Etudiant getEtudiantByCode(long code) {
        Optional<Etudiant> found =
                list.stream().filter(e -> e.getCode() == code).findFirst();
        return found.isPresent() ? found.get() : null;
    }

    @Override
    public List<Etudiant> getAllEtudiant() {
        return list;
    }
}
