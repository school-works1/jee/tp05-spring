package services;

import dao.Etudiant;
import dao.ImplEtudiant;

import java.util.List;

public class ImplEtudiantService implements EtudiantService{
    private ImplEtudiant dao;

    public ImplEtudiantService() {
        dao = new ImplEtudiant();
    }

    public ImplEtudiant getDao() {
        return dao;
    }

    public void setDao(ImplEtudiant dao) {
        this.dao = dao;
    }

    public void initialiser() {

    }

    @Override
    public Etudiant addEtudiant(Etudiant e) {
        return dao.addEtudiant(e);
    }

    @Override
    public void deleteEtudiant(long code) {
        dao.deleteEtudiant(code);
    }

    @Override
    public Etudiant updateEtudiant(Etudiant e) {
        return dao.updateEtudiant(e);
    }

    @Override
    public Etudiant getEtudiantByCode(long code) {
        return dao.getEtudiantByCode(code);
    }

    @Override
    public List<Etudiant> getAllEtudiant() {
        return dao.getAllEtudiant();
    }
}
