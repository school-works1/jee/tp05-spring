package services;

import dao.Etudiant;

import java.util.List;

public interface EtudiantService {

    Etudiant addEtudiant(Etudiant e);
    void  deleteEtudiant(long code);
    Etudiant updateEtudiant(Etudiant e);
    Etudiant getEtudiantByCode(long code);
    List<Etudiant> getAllEtudiant();
}
