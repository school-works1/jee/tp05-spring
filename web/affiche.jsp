<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ablil
  Date: 1/29/21
  Time: 3:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liste des etudiant</title>
</head>
<body>
    <h1>La liste des etudiants</h1>
    <table border="2">
        <thead>
        <th>
            <td>Matricule</td>
            <td>Nom</td>
            <td>Prenom</td>
            <td>Age</td>
        </th>
        </thead>
        <tbody>
            <c:forEach items="${liste}" var="etudiant">
                <tr>
                    <td>${etudiant.code}</td>
                    <td>${etudiant.nom}</td>
                    <td>${etudiant.prenom}</td>
                    <td>${etudiant.age}</td>
                    <td><a href="delete.html?code=${etudiant.code}">Supprimer
                    </a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <a href="index.html">Revenir a la page d'acceuil</a>
    <a href="saisir.html">Saisir un nouveau etudiant</a>
</body>
</html>
